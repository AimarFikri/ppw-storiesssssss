
from django.urls import path
from . import views
from django.contrib.auth import logout
from django.contrib.auth import views as auth_views

app_name = 'login'

urlpatterns = [
    path('welcome/', views.welcome, name='welcome'),
    path('login/', views.loginPage, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logoutPage , name='logout'),
]
