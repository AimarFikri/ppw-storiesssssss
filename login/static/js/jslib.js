
var ketikan = $("#keyword").val();
var container_books = $('.containerBooks');
var baris_tabel = $('.baris_tabel');
$.ajax(
    {
        url: "/libary/books/" + "?q=" + "makanan",
        success: function (data) {
            var array_items = data.items;
            for (i = 0; i < array_items.length; i++) {
                        var items_itu = array_items[i];
                        var urutan = i + 1;
                        var judul = items_itu.volumeInfo.title;
                        var gambar = items_itu.volumeInfo.imageLinks;
                        var author_array = items_itu.volumeInfo.authors;

                        if(gambar) gambar = gambar.thumbnail;
                        else gambar = "https://media1.tenor.com/images/b213dc57bfa0ff88ca7522139c937acf/tenor.gif";

                        if(author_array === undefined) author_array = "Tidak tercantum";

                        baris_tabel.append(
                            "<tr>" +
                            "<th>" + urutan + "</th>" +
                            "<td>" + "<img class='gambar_buku' src=" + gambar + "></td>" +
                            "<td>" + judul + "</td>" +
                            "<td>" + author_array + "</td>" +
                            "</tr>");
                    }
        }

    }
)

$("#button_cari").click(function () {
    var ketikan = $("#keyword").val();
    var baris_tabel = $('.baris_tabel');
    $.ajax(
        {
            url: "/libary/books/" + "?q=" + ketikan,
            success: function (data) {
                console.log(data);
                var array_items = data.items;
                baris_tabel.empty();
                for (i = 0; i < array_items.length; i++) {
                    var items_itu = array_items[i];
                    var urutan = i + 1;
                    var judul = items_itu.volumeInfo.title;
                    var gambar = items_itu.volumeInfo.imageLinks;
                    var author_array = items_itu.volumeInfo.authors;

                    if(gambar) gambar = gambar.thumbnail;
                    else gambar = "https://media1.tenor.com/images/b213dc57bfa0ff88ca7522139c937acf/tenor.gif";

                    if(author_array === undefined) author_array = "Tidak tercantum";

                    baris_tabel.append(
                        "<tr>" +
                        "<th>" + urutan + "</th>" +
                        "<td>" + "<img class='gambar_buku' src=" + gambar + "></td>" +
                        "<td>" + judul + "</td>" +
                        "<td>" + author_array + "</td>" +
                        "</tr>");
                }
            }
        }
    )


})

