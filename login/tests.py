
from django.test import TestCase, Client
from django.contrib.auth.models import User

LOG_IN_URL = '/login/'


# Create your tests here.
class UnitTest(TestCase):
    # Test untuk mengetahui apakah URL telah tersedia
    def setUp(self):
        user = User.objects.create_user(username='aimar', password='aimarpass')
        user.save()

    def test_url_login_is_exist(self):
        response = Client().get(LOG_IN_URL)
        self.assertEqual(response.status_code, 200)

    def test_url_welcome_is_exist(self):
        self.client.login(username='aimar', password='aimarpass')
        response = self.client.get('/welcome/')
        self.assertEqual(response.status_code, 200)

    def test_url_register_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = self.client.get(LOG_IN_URL)
        self.assertTemplateUsed(response, "login.html")

    # def test_welcome_using_template(self):
    #     self.client.login(username='aimar', password='Aimarpass')
    #     response = Client().get('/welcome/')
    #     self.assertTemplateUsed(response, "welcome.html")

    def test_register_using_template(self):
        response = self.client.get('/register/')
        self.assertTemplateUsed(response, "register.html")

    def test_create_new_user_from_url(self):
        total_user = User.objects.all().count()
        passed_data = {
            'username': 'thisisusername',
            'password1': 'ThisIsAP@ssword123',
            'password2': 'ThisIsAP@ssword123',
        }
        response = self.client.post('/register/', data=passed_data)
        self.assertEqual(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), total_user + 1)

    def test_success_signin_aimar(self):
        passed_data = {
            'username': 'aimar',
            'password': 'aimarpass'
        }
        response = self.client.post(LOG_IN_URL, data=passed_data)
        self.assertEquals(response.status_code, 302)

    def test_failed_signin_aimar(self):
        passed_data = {
            'username': 'aimar',
            'password': 'aimarpass1'
        }
        response = self.client.post(LOG_IN_URL, data=passed_data)
        self.assertEquals(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Username OR password is incorrect')


    def test_logout_user(self):
        self.client.login(username='pass', password='Aimarpass')
        response = self.client.get('/logout/')
        self.assertEquals(response.status_code, 302)

