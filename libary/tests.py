from django.test import TestCase

from django.test import TestCase, Client


# Create your tests here.
class UnitTest(TestCase):
    # Test untuk mengetahui apakah URL telah tersedia
    def test_url_is_exist(self):
        response = Client().get('/libary/')
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = self.client.get("/libary/")
        self.assertTemplateUsed(response, "libary.html")

    def test_api_google(self):
        response = self.client.get("/libary/books/?q=Aimar")
        self.assertEqual(response.status_code, 200)
        content = response.content.decode("utf-8")
        self.assertIn("items", content)
        self.assertEqual(response['content-type'], 'application/json')

