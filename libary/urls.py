from django.urls import path
from . import views

app_name = 'libary'

urlpatterns = [
    path('', views.libarry, name='libarry'),
    path('books/', views.books, name="books"),
]
