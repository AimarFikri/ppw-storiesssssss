from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import requests
import json

# Create your views here.
def libarry(request):
    return render(request, "libary.html")

def books(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data_books = json.loads(ret.content)
    return JsonResponse(data_books, safe=False)
